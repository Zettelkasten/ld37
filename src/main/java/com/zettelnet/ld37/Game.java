package com.zettelnet.ld37;

import com.zettelnet.ld37.customer.CustomerManager;
import com.zettelnet.ld37.input.Keyboard;
import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.render.DeviceRenderer;
import com.zettelnet.ld37.render.GameRenderer;
import com.zettelnet.ld37.render.HelpRenderer;
import com.zettelnet.ld37.render.ItemRenderer;
import com.zettelnet.ld37.render.LevelRenderer;
import com.zettelnet.ld37.room.BurgerBread;
import com.zettelnet.ld37.room.BurgerBuilder;
import com.zettelnet.ld37.room.Calendar;
import com.zettelnet.ld37.room.Clock;
import com.zettelnet.ld37.room.Cooktop;
import com.zettelnet.ld37.room.CustomerSlot;
import com.zettelnet.ld37.room.CuttingBoard;
import com.zettelnet.ld37.room.Fridge;
import com.zettelnet.ld37.room.FriesBox;
import com.zettelnet.ld37.room.Fryolator;
import com.zettelnet.ld37.room.OrderSlot;
import com.zettelnet.ld37.room.Potatoes;
import com.zettelnet.ld37.room.RecipeSlot;
import com.zettelnet.ld37.room.Room;
import com.zettelnet.ld37.room.Sauce;
import com.zettelnet.ld37.room.Tomatoes;
import com.zettelnet.ld37.room.Trash;
import com.zettelnet.ld37.room.Tray;
import com.zettelnet.zetlib.loop.Loop;
import com.zettelnet.zetlib.loop.UpdateLoop;
import com.zettelnet.zetlib.opengl.ModelManager;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class Game {

	private final GameRenderer renderer = new GameRenderer(this);
	private final ModelManager modelManager = new ModelManager();

	private final Loop updateLoop = new UpdateLoop(null, this::update, 20);

	private final Room room = new Room(this);
	private ItemRenderer itemRenderer;
	private DeviceRenderer deviceRenderer;

	private final CustomerManager customerManager = new CustomerManager(room);

	private boolean showHelp = true, showCalendar = true;

	private Calendar calendar;
	
	public void init() {
		renderer.addRenderer(modelManager);
		renderer.addKeyListener((long window, int key, int scancode, int action, int mods) -> {
			if (action == GLFW_RELEASE && key == GLFW_KEY_ESCAPE) {
				setRunning(false);
			}
		});

		// Cooktop
		room.addDevice(Cooktop.makeHelper(room, 242, 483, 119, 112));
		room.addDevice(Cooktop.makeHelper(room, 242, 483 + 112, 119, 112));
		room.addDevice(Cooktop.makeHelper(room, 242 + 112, 483, 119, 112));
		room.addDevice(Cooktop.makeHelper(room, 242 + 112, 483 + 112, 119, 112));
		// Fridge
		room.addDevice(Fridge.makeHelper(room, 1200, 266, 200, 270));
		// Trash
		room.addDevice(Trash.makeHelper(room, 1059, 931, 82, 82));
		// Potatoes
		room.addDevice(Potatoes.makeHelper(room, 227, 209, 119, 125));
		// Cutting Board
		room.addDevice(CuttingBoard.makeHelper(room, 575, 194, 160, 100));
		// Fryolator
		room.addDevice(Fryolator.makeHelper(room, 225, 350, 115, 90));
		room.addDevice(Fryolator.makeHelper(room, 358, 346, 115, 90));
		// Fries Box
		room.addDevice(FriesBox.makeHelper(room, 361, 213, 90, 90));
		// Bread
		room.addDevice(BurgerBread.makeHelper(room, 559, 912, 82, 136));
		// Burger Builders
		room.addDevice(BurgerBuilder.makeHelper(room, 638, 905, 75, 150));
		room.addDevice(BurgerBuilder.makeHelper(room, 638 + 1 * 75, 905, 75, 150));
		room.addDevice(BurgerBuilder.makeHelper(room, 638 + 2 * 75, 905, 75, 150));
		// Top
		room.addDevice(BurgerBuilder.makeHelper(room, 767, 182, 75, 150));
		room.addDevice(BurgerBuilder.makeHelper(room, 767 + 75, 182, 75, 150));
		// Tomatoes
		room.addDevice(Tomatoes.makeHelper(room, 440, 199, 98, 87));
		// Sauces
		room.addDevice(Sauce.makeHelper(room, 1218, 918, 50, 120, new Item(ItemType.SAUCE_RED)));
		room.addDevice(Sauce.makeHelper(room, 1218 + 50, 918 - 10, 50, 120, new Item(ItemType.SAUCE_YELLOW)));

		// Trays
		room.addDevice(Tray.makeHelper(room, 1272, 490, 160, 120, customerManager, 2));
		room.addDevice(Tray.makeHelper(room, 1272, 490 + 135 * 1, 160, 120, customerManager, 1));
		room.addDevice(Tray.makeHelper(room, 1272, 490 + 135 * 2, 160, 120, customerManager, 0));
		// Customers
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200, 490, 160, 120, customerManager, 2));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200, 490 + 135 * 1, 160, 120, customerManager, 1));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200, 490 + 135 * 2, 160, 120, customerManager, 0));
		// Orders
		room.addDevice(OrderSlot.makeHelper(room, 1272 + 200 + 220, 490, 340, 120, customerManager, 2));
		room.addDevice(OrderSlot.makeHelper(room, 1272 + 200 + 220, 490 + 135 * 1, 340, 120, customerManager, 1));
		room.addDevice(OrderSlot.makeHelper(room, 1272 + 200 + 220, 490 + 135 * 2, 340, 120, customerManager, 0));
		// Waiting Customers
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200, 490 - 135 * 1, 160, 120, customerManager, 3));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200, 490 - 135 * 2, 160, 120, customerManager, 4));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200, 490 - 135 * 3, 160, 120, customerManager, 5));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200 + 160 * 1, 490 - 135 * 1, 160, 120, customerManager, 8));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200 + 160 * 1, 490 - 135 * 2, 160, 120, customerManager, 7));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200 + 160 * 1, 490 - 135 * 3, 160, 120, customerManager, 6));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200 + 160 * 2, 490 - 135 * 1, 160, 120, customerManager, 9));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200 + 160 * 2, 490 - 135 * 2, 160, 120, customerManager, 10));
		room.addDevice(CustomerSlot.makeHelper(room, 1272 + 200 + 160 * 2, 490 - 135 * 3, 160, 120, customerManager, 11));
		// Recipe
		room.addDevice(RecipeSlot.makeHelper(room, 1272 + 110 + 250, 490 + 135 * 3 + 40, 470, 200, customerManager));

		// Clock
		room.addDevice(Clock.makeHelper(room, 154, 904, 160, 160));
		// Calendar
		this.calendar = room.addDevice(Calendar.makeHelper(room, 1920 / 2, 1080 / 2, 600, 500));
		
		
		renderer.addKeyListener(new Keyboard(room));
		renderer.addMouseListener(new Mouse(room));

		itemRenderer = new ItemRenderer(modelManager, room);
		deviceRenderer = new DeviceRenderer(modelManager, room);
		renderer.addRenderer(new LevelRenderer(modelManager));
		renderer.addRenderer(deviceRenderer);
		renderer.addRenderer(itemRenderer);
		renderer.addRenderer(new HelpRenderer(modelManager, room));
	}

	public void update() {
		if (!showHelp && !showCalendar) {
			room.update(1F);
			customerManager.update(1F);
		}
	}

	public void destroy() {
		renderer.destroy();
	}

	public boolean isShowHelp() {
		return showHelp;
	}
	
	public boolean isShowCalendar() {
		return showCalendar;
	}
	
	public void setShowCalendar(boolean showCalendar) {
		this.showCalendar = showCalendar;
	}
	
	public boolean isShowSomething() {
		return showHelp || showCalendar;
	}
	
	public void setShowHelp(boolean showHelp) {
		this.showHelp = showHelp;
	}

	public void setRunning(boolean running) {
		renderer.setRunning(running);
		updateLoop.setRunning(running);

		if (running == false) {
			destroy();
		}
	}

	public Loop getUpdateLoop() {
		return updateLoop;
	}

	public GameRenderer getGameRenderer() {
		return renderer;
	}

	public ModelManager getModelManager() {
		return modelManager;
	}

	public ItemRenderer getItemRenderer() {
		return itemRenderer;
	}

	public DeviceRenderer getDeviceRenderer() {
		return deviceRenderer;
	}
	
	public Calendar getCalendar() {
		return calendar;
	}
	
	public CustomerManager getCustomerManager() {
		return customerManager;
	}
}
