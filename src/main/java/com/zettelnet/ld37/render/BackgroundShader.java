package com.zettelnet.ld37.render;

import com.zettelnet.zetlib.opengl.ShaderProgram;

public class BackgroundShader extends ShaderProgram {

	private static final String VERTEX_FILE = "/backgroundv.glsl";
	private static final String FRAGMENT_FILE = "/backgroundf.glsl";

	public BackgroundShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}
}
