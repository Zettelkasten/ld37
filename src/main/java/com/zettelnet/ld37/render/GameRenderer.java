package com.zettelnet.ld37.render;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import com.zettelnet.ld37.Game;
import com.zettelnet.zetlib.input.KeyAdapter;
import com.zettelnet.zetlib.input.KeyCallback;
import com.zettelnet.zetlib.loop.LoopCallback;
import com.zettelnet.zetlib.loop.LoopInitializer;
import com.zettelnet.zetlib.loop.RenderLoop;
import com.zettelnet.zetlib.opengl.Renderer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;

public class GameRenderer implements LoopInitializer, LoopCallback {

	public static final boolean FULLSCREEN = true;

	public static int WINDOW_WIDTH = 10, WINDOW_HEIGHT = 10;

	private final Game game;

	private final RenderLoop loop = new RenderLoop(this, this);
	private final KeyAdapter keyAdapter = new KeyAdapter();
	private final MouseAdapter mouseAdapter = new MouseAdapter();

	private final List<Renderer> renderers = new ArrayList<>();

	public static long WINDOW;

	public GameRenderer(final Game game) {
		this.game = game;
	}

	public boolean setRunning(boolean running) {
		return loop.setRunning(running);
	}

	@Override
	public void init() {
		GLFWErrorCallback.createPrint(System.err).set();

		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		// Get the resolution of the primary monitor
		final int scale = FULLSCREEN ? 1 : 2 / 1; 
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		WINDOW_WIDTH = vidmode.width() / scale;
		WINDOW_HEIGHT = vidmode.height() / scale;
		
		// Create the window
		WINDOW = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Bob's Burgers", FULLSCREEN ? glfwGetPrimaryMonitor() : NULL, NULL);
		if (WINDOW == NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		glfwSetKeyCallback(WINDOW, keyAdapter);
		this.mouseAdapter.setWindow(WINDOW);
		glfwSetMouseButtonCallback(WINDOW, mouseAdapter);

		
		// Center our window
		glfwSetWindowPos(WINDOW, (vidmode.width() - WINDOW_WIDTH) / 2, (vidmode.height() - WINDOW_HEIGHT) / 2);
		
		// Make the OpenGL context current
		glfwMakeContextCurrent(WINDOW);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(WINDOW);

		GL.createCapabilities();

		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

		for (Renderer renderer : renderers) {
			renderer.init();
		}
	}

	@Override
	public void destroy() {
		for (Renderer renderer : renderers) {
			renderer.destroy();
		}

		glfwFreeCallbacks(WINDOW);
		glfwDestroyWindow(WINDOW);

		glfwTerminate();

		GLFWErrorCallback callback = glfwSetErrorCallback(null);
		if (callback != null) {
			callback.free();
		}
	}

	public void onCycle() {
		if (glfwWindowShouldClose(WINDOW)) {
			game.setRunning(false);
		}

		double time = game.getUpdateLoop().getCycleProgress(System.currentTimeMillis());

		glfwPollEvents();

		// clear the framebuffer & swap color buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		for (Renderer renderer : renderers) {
			renderer.render(time);
		}

		glfwSwapBuffers(WINDOW);
	}

	public void addRenderer(Renderer renderer) {
		this.renderers.add(renderer);
	}

	public void addKeyListener(KeyCallback callback) {
		keyAdapter.registerCallback(callback);
	}

	public void addMouseListener(MouseCallback callback) {
		mouseAdapter.registerCallback(callback);
	}

	public KeyAdapter getKeyAdapter() {
		return keyAdapter;
	}

	public MouseAdapter getMouseAdapter() {
		return mouseAdapter;
	}
}
