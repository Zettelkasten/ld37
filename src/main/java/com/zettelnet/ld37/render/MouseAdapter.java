package com.zettelnet.ld37.render;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWMouseButtonCallbackI;

public class MouseAdapter implements GLFWMouseButtonCallbackI {

	private final Set<MouseCallback> callbacks = new HashSet<>();
	private long window;
	
	public void setWindow(long window) {
		this.window = window;
	}
	
	public void registerCallback(MouseCallback callback) {
		this.callbacks.add(callback);
	}

	@Override
	public void invoke(long window, int button, int action, int mods) {
		// get mouse location
		double[] x = new double[1];
		double[] y = new double[1];

		GLFW.glfwGetCursorPos(window, x, y);

		for (MouseCallback controller : callbacks) {
			controller.invoke(window, x[0], y[0], button, action, mods);
		}
	}
	
	public double getMouseX() {
		double[] x = new double[1];
		double[] y = new double[1];

		GLFW.glfwGetCursorPos(window, x, y);
		return x[0];
	}
	
	public double getMouseY() {
		double[] x = new double[1];
		double[] y = new double[1];

		GLFW.glfwGetCursorPos(window, x, y);
		return y[0];
	}
}
