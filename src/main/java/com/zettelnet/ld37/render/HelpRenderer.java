package com.zettelnet.ld37.render;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import com.zettelnet.ld37.room.Room;
import com.zettelnet.zetlib.opengl.ModelManager;
import com.zettelnet.zetlib.opengl.ModelUtils;
import com.zettelnet.zetlib.opengl.Renderer;
import com.zettelnet.zetlib.opengl.ShaderProgram;
import com.zettelnet.zetlib.opengl.TexturedModel;

public class HelpRenderer implements Renderer {

	private final ModelManager modelManager;
	private final Room room;

	private ShaderProgram helpShader;
	private TexturedModel help;
	private ShaderProgram backgroundShader;
	private TexturedModel background;

	public HelpRenderer(final ModelManager modelManager, final Room room) {
		this.modelManager = modelManager;
		this.room = room;
	}

	@Override
	public void init() {
		helpShader = new HelpShader();
		help = ModelUtils.createSprite(modelManager, "/help.png", 0, 0, 1, 1, helpShader);
		backgroundShader = new BackgroundShader();
		background = ModelUtils.createSprite(modelManager, "/overlay.png", 0, 0, 1, 1, backgroundShader);
	}

	@Override
	public void render(double time) {
		if (room.getGame().isShowHelp()) {
			helpShader.enable();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			modelManager.render(help);
			glDisable(GL_BLEND);

			helpShader.disable();
		}
		if (room.getGame().isShowCalendar()) {
			backgroundShader.enable();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			modelManager.render(background);
			glDisable(GL_BLEND);

			backgroundShader.disable();

			int calendarOffset = 0;
			if (room.isDefeat()) {
				calendarOffset = 1;
			}
			room.getGame().getDeviceRenderer().renderDevice(time, room.getGame().getCalendar(), 1038 + 600 * calendarOffset, 440, 600, 500);

			if (!room.isDefeat()) {
				int x = 0;
				int y = 1;

				int level = room.getLevel();
				if (room.isWon()) {
					level = 7;
				}
				if (level < 4) {
					x = level;
					y = 1;
				} else {
					x = level - 4;
					y = 2;

				}

				room.getGame().getDeviceRenderer().renderDevice(time, room.getGame().getCalendar(), 1038 + 600 * x, 440 + 500 * y, 600, 500);
			}
		}
	}

	@Override
	public void destroy() {
		helpShader.cleanUp();
	}
}
