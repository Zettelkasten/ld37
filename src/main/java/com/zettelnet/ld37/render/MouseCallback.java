package com.zettelnet.ld37.render;

@FunctionalInterface
public interface MouseCallback {

	void invoke(long window, double x, double y, int button, int action, int mods);
}
