package com.zettelnet.ld37.render;

import com.zettelnet.zetlib.opengl.ShaderProgram;

public class DeviceShader extends ShaderProgram {

	private static final String VERTEX_FILE = "/devicev.glsl";
	private static final String FRAGMENT_FILE = "/devicef.glsl";

	public DeviceShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}
}
