package com.zettelnet.ld37.render;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;

import org.joml.Matrix4f;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Amountable;
import com.zettelnet.ld37.item.BurgerItem;
import com.zettelnet.ld37.item.Doable;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.room.Room;
import com.zettelnet.zetlib.opengl.ModelManager;
import com.zettelnet.zetlib.opengl.ModelUtils;
import com.zettelnet.zetlib.opengl.RenderUtils;
import com.zettelnet.zetlib.opengl.Renderer;
import com.zettelnet.zetlib.opengl.ShaderProgram;
import com.zettelnet.zetlib.opengl.TexturedModel;

public class ItemRenderer implements Renderer {

	private final Room room;

	private final ModelManager modelManager;

	private ShaderProgram itemShader;
	private TexturedModel itemModel;

	private int transformUniform, textureSizeUniform, textureOffsetUniform, brightnessUniform;

	public static int SPRITESHEET_WIDTH = 1920, SPRITESHEET_HEIGHT = 1080;
	public static int SPRITESHEET_ITEM_WIDTH = 80, SPRITESHEET_ITEM_HEIGHT = 80;

	public static float ITEM_SPRITE_WIDTH = (float) SPRITESHEET_ITEM_WIDTH / SPRITESHEET_WIDTH;
	public static float ITEM_SPRITE_HEIGHT = (float) SPRITESHEET_ITEM_HEIGHT / SPRITESHEET_HEIGHT;

	public ItemRenderer(final ModelManager modelManager, final Room room) {
		this.modelManager = modelManager;
		this.room = room;
	}

	@Override
	public void init() {
		itemShader = new ItemShader();
		itemModel = ModelUtils.createSprite(modelManager, "/items.png", 0, 0, ITEM_SPRITE_WIDTH, ITEM_SPRITE_HEIGHT, itemShader);

		transformUniform = itemShader.getUniformLocation("transform");
		textureSizeUniform = itemShader.getUniformLocation("textureSize");
		textureOffsetUniform = itemShader.getUniformLocation("textureOffset");
		brightnessUniform = itemShader.getUniformLocation("brightness");
	}

	@Override
	public void render(double time) {
		if (room.getInventory() != null) {
			renderItem(time, room.getInventory(), (float) Mouse.getPosX(), (float) Mouse.getPosY());
		}
	}

	public void renderItem(double time, Item item, float x, float y) {
		renderItem(time, item, x, y, false);
	}

	public void renderItem(double time, Item item, float x, float y, boolean asBurger) {
		ItemType type = item.getType();
		if (type == ItemType.BURGER) {
			renderBurger(time, (BurgerItem) item, x, y);
			return;
		}

		float width = asBurger ? 1 : type.getWidth();
		float height = asBurger ? 1 : type.getHeight();
		
		itemShader.enable();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		Matrix4f transform = new Matrix4f()
				.identity()
				.translate(x, y, 0)
				.scale(0.15F / 2 * width, 0.15F * height, 0.15F);
		glUniformMatrix4fv(transformUniform, false, RenderUtils.createFloatBuffer(transform));

		int textureX = type.getX();
		int textureY = type.getY();
		if (asBurger) {
			textureX = type.getBurgerX();
			textureY = type.getBurgerY();
		}
		if (item instanceof Amountable) {
			textureX += ((Amountable) item).getAmountOffset();
		}

		glUniform2f(textureSizeUniform, width, height);
		glUniform2f(textureOffsetUniform,
				ITEM_SPRITE_WIDTH * textureX,
				ITEM_SPRITE_HEIGHT * textureY);
		float brightness = 0;
		if (item instanceof Doable) {
			brightness = -((Doable) item).getDoneness() / 5F + 0.2F;
		}
		glUniform1f(brightnessUniform, brightness);

		modelManager.render(itemModel);
		glDisable(GL_BLEND);
		itemShader.disable();
	}
	
	public void renderBurger(double time, BurgerItem burger, float x, float y) {
		float offset = 0;
		for (Item item : burger.getItems()) {
			renderItem(time, item, x, y + offset, true);
			offset += item.getType().getBurgerHeight();
		}
	}

	@Override
	public void destroy() {
		itemShader.cleanUp();
	}

}
