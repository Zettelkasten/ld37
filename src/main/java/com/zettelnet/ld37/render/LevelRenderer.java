package com.zettelnet.ld37.render;

import com.zettelnet.zetlib.opengl.ModelManager;
import com.zettelnet.zetlib.opengl.ModelUtils;
import com.zettelnet.zetlib.opengl.Renderer;
import com.zettelnet.zetlib.opengl.ShaderProgram;
import com.zettelnet.zetlib.opengl.TexturedModel;

public class LevelRenderer implements Renderer {

	private final ModelManager modelManager;

	private ShaderProgram backgroundShader;
	private TexturedModel background;

	public LevelRenderer(final ModelManager modelManager) {
		this.modelManager = modelManager;
	}

	@Override
	public void init() {
		backgroundShader = new HelpShader();
		background = ModelUtils.createSprite(modelManager, "/background.png", 0, 0, 1, 1, backgroundShader);
	}

	@Override
	public void render(double time) {
		backgroundShader.enable();

		modelManager.render(background);

		backgroundShader.disable();
	}

	@Override
	public void destroy() {
		backgroundShader.cleanUp();
	}
}
