package com.zettelnet.ld37.render;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;

import org.joml.Matrix4f;

import com.zettelnet.ld37.room.Device;
import com.zettelnet.ld37.room.Room;
import com.zettelnet.zetlib.opengl.ModelManager;
import com.zettelnet.zetlib.opengl.ModelUtils;
import com.zettelnet.zetlib.opengl.RenderUtils;
import com.zettelnet.zetlib.opengl.Renderer;
import com.zettelnet.zetlib.opengl.ShaderProgram;
import com.zettelnet.zetlib.opengl.TexturedModel;

public class DeviceRenderer implements Renderer {

	private final Room room;

	private final ModelManager modelManager;

	private ShaderProgram deviceShader;
	private TexturedModel deviceModel;

	private int transformUniform, textureSizeUniform, textureOffsetUniform;
	private int angreenessUniform;

	public static int SPRITESHEET_WIDTH = 1920 * 2, SPRITESHEET_HEIGHT = 1080 * 2;
	public static int SPRITESHEET_PAN_WIDTH = 119, SPRITESHEET_PAN_HEIGHT = 112;

	public static float DEVICE_SPRITE_WIDTH = (float) SPRITESHEET_PAN_WIDTH / SPRITESHEET_WIDTH;
	public static float DEVICE_SPRITE_HEIGHT = (float) SPRITESHEET_PAN_HEIGHT / SPRITESHEET_HEIGHT;

	public DeviceRenderer(final ModelManager modelManager, final Room room) {
		this.modelManager = modelManager;
		this.room = room;
	}

	@Override
	public void init() {
		deviceShader = new DeviceShader();
		deviceModel = ModelUtils.createSprite(modelManager, "/devices.png", 0, 0, 1, 1, deviceShader);

		transformUniform = deviceShader.getUniformLocation("transform");
		textureSizeUniform = deviceShader.getUniformLocation("textureSize");
		textureOffsetUniform = deviceShader.getUniformLocation("textureOffset");
		angreenessUniform = deviceShader.getUniformLocation("angreeness");
	}

	@Override
	public void render(double time) {
		for (Device device : room.getDevices()) {
			renderDevice(time, device);
		}
	}

	public void renderDevice(double time, Device device) {
		device.render(time);
	}

	public void renderDevice(double time, Device device, int spriteX, int spriteY, int spriteWidth, int spriteHeight) {
		renderDevice(time, device, spriteX, spriteY, spriteWidth, spriteHeight, 0F, 0F);
	}

	public void renderDevice(double time, Device device, int spriteX, int spriteY, int spriteWidth, int spriteHeight, float renderOffsetX, float renderOffsetY) {
		renderDevice(time, device, spriteX, spriteY, spriteWidth, spriteHeight, renderOffsetX, renderOffsetY, 0F);
	}

	public void renderDevice(double time, Device device, int spriteX, int spriteY, int spriteWidth, int spriteHeight, float renderOffsetX, float renderOffsetY, float angreeness) {
		renderDevice(time, device, spriteX, spriteY, spriteWidth, spriteHeight, renderOffsetX, renderOffsetY, angreeness, 0F);
	}

	public void renderDevice(double time, Device device, int spriteX, int spriteY, int spriteWidth, int spriteHeight, float renderOffsetX, float renderOffsetY, float angreeness, float rotation) {
		deviceShader.enable();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		float w = (float) spriteWidth / SPRITESHEET_WIDTH;
		float h = (float) spriteHeight / SPRITESHEET_HEIGHT;
		Matrix4f transform = new Matrix4f()
				.identity()
				.translate((float) device.getX() + renderOffsetX, (float) device.getY() + renderOffsetY, 0)
				.scale(w * 4, h * 4, 0)
				.rotate(rotation, 0F, 0F, 1F);
		glUniformMatrix4fv(transformUniform, false, RenderUtils.createFloatBuffer(transform));

		glUniform2f(textureSizeUniform, w, h);
		glUniform2f(textureOffsetUniform, (float) spriteX / SPRITESHEET_WIDTH, (float) spriteY / SPRITESHEET_HEIGHT);
		glUniform1f(angreenessUniform, angreeness / 2F);

		modelManager.render(deviceModel);

		glDisable(GL_BLEND);
		deviceShader.disable();
	}

	public void renderNumber(double time, float x, float y, String str) {
		int size = str.length();

		float xOffset = (DIGIT_WIDTH * size + 0.02F * (size - 1)) / 2;
		x -= xOffset;

		for (char c : str.toCharArray()) {
			renderDigit(time, x, y, c);
			x += DIGIT_WIDTH + 0.02F;
		}
	}

	private final float DIGIT_WIDTH = (float) 40 / SPRITESHEET_WIDTH;
	private final float DIGIT_HEIGHT = (float) 50 / SPRITESHEET_HEIGHT;

	public void renderDigit(double time, float x, float y, char c) {
		deviceShader.enable();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		Matrix4f transform = new Matrix4f()
				.identity()
				.translate(x, y, 0)
				.scale(DIGIT_WIDTH * 4, DIGIT_HEIGHT * 4, 0);
		glUniformMatrix4fv(transformUniform, false, RenderUtils.createFloatBuffer(transform));

		int index = charIndex(c);
		final int spriteX = index * 40;
		final int spriteY = 922;

		glUniform2f(textureSizeUniform, DIGIT_WIDTH, DIGIT_HEIGHT);
		glUniform2f(textureOffsetUniform, (float) spriteX / SPRITESHEET_WIDTH, (float) spriteY / SPRITESHEET_HEIGHT);
		glUniform1f(angreenessUniform, 0F);

		modelManager.render(deviceModel);

		glDisable(GL_BLEND);
		deviceShader.disable();
	}

	public int charIndex(char c) {
		switch (c) {
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		case '+':
			return 10;
		case '-':
			return 11;
		case '%':
			return 12;
		case '(':
			return 13;
		case ')':
			return 14;
		case '$':
			return 15;
		default:
			return 0;
		}
	}

	@Override
	public void destroy() {
		deviceShader.cleanUp();
	}

}
