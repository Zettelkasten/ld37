package com.zettelnet.ld37.render;

import com.zettelnet.zetlib.opengl.ShaderProgram;

public class ItemShader extends ShaderProgram {

	private static final String VERTEX_FILE = "/itemv.glsl";
	private static final String FRAGMENT_FILE = "/itemf.glsl";

	public ItemShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}
}
