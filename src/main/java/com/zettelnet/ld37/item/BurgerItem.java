package com.zettelnet.ld37.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BurgerItem extends Item {

	private final List<Item> items;

	public BurgerItem(final Item... items) {
		this(new ArrayList<>(Arrays.asList(items)));
	}

	public BurgerItem(final List<Item> items) {
		super(ItemType.BURGER);
		this.items = items;
	}

	public List<Item> getItems() {
		return items;
	}

	public boolean isDone() {
		if (items.isEmpty()) {
			return false;
		}
		Item top = items.get(items.size() - 1);
		return top.getType() == ItemType.BREAD_TOP;
	}

	public void add(Item item) {
		items.add(item);
	}

	@Override
	public float getHappinessFactor() {
		float factor = 1F;
		for (Item item : items) {
			factor *= item.getHappinessFactor();
		}
		return factor;
	}

	@Override
	public int getPrice() {
		int price = 0;
		for (Item item : items) {
			price += item.getPrice();
		}
		
		if (items.size() > 1) {
			price += price * (items.size() - 1) * 0.2F;
		}
		return price;
	}
}
