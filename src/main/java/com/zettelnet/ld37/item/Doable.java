package com.zettelnet.ld37.item;

public interface Doable {

	float getDoneness();
	
	void increaseDoneness(float amount);
}
