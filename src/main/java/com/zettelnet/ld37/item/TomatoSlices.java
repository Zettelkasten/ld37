package com.zettelnet.ld37.item;

public class TomatoSlices extends Item implements Amountable {

	private int amount;

	public TomatoSlices() {
		super(ItemType.TOMATO_SLICES);
		this.amount = 3;
	}

	public int getAmount() {
		return amount;
	}
	
	@Override
	public int getAmountOffset() {
		return 3 - amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
