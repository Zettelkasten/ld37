package com.zettelnet.ld37.item;

public class Items {

	public static boolean equals(Item a, Item b) {
		if (a == null || b == null) {
			return false;
		}
		if (a.getType() != b.getType()) {
			return false;
		}
		if (a.getType() == ItemType.BURGER) {
			BurgerItem aBurger = (BurgerItem) a;
			BurgerItem bBurger = (BurgerItem) b;
			
			if (aBurger.getItems().size() != bBurger.getItems().size()) {
				return false;
			}
			for (int i = 0; i < aBurger.getItems().size(); i++) {
				if (!equals(aBurger.getItems().get(i), bBurger.getItems().get(i))) {
					return false;
				}
			}
		}
		return true;
	}
}
