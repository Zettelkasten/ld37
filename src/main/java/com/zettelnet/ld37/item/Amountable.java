package com.zettelnet.ld37.item;

public interface Amountable {

	int getAmount();

	int getAmountOffset();
}
