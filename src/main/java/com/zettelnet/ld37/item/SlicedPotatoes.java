package com.zettelnet.ld37.item;

public class SlicedPotatoes extends Item implements Doable {

	public SlicedPotatoes() {
		super(ItemType.SLICED_POTATOES);
	}

	private float doneness;

	public float getDoneness() {
		return doneness;
	}

	public void increaseDoneness(float by) {
		if (this.doneness < 3) {
			this.doneness += by;
		}
	}
}
