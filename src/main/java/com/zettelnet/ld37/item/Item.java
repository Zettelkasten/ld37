package com.zettelnet.ld37.item;

public class Item {

	private final ItemType type;
	
	public Item(ItemType type) {
		this.type = type;
	}

	public ItemType getType() {
		return type;
	}
	
	public float getHappinessFactor() {
		return 1F;
	}
	
	public int getPrice() {
		return type.getBasePrice();
	}
}
