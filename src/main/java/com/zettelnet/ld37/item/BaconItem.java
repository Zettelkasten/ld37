package com.zettelnet.ld37.item;

public class BaconItem extends Item implements Roastable {

	public BaconItem() {
		super(ItemType.BACON);
	}

	private float doneness;

	public float getDoneness() {
		return doneness;
	}

	public void increaseDoneness(float by) {
		if (this.doneness < 3) {
			this.doneness += by;
		}
	}

	@Override
	public float getHappinessFactor() {
		if (doneness > 1) {
			return Math.max((2 - doneness) * 2, 0.1F);
		} else {
			return doneness;
		}
	}
}
