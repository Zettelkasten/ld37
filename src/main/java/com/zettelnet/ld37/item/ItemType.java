package com.zettelnet.ld37.item;

public enum ItemType {

	MEAT(4, 1, 0.025F, true, 3), BREAD(1, 0, 0.035F, true, 0), BREAD_BOTTOM(2, 0, 0.03F, true, 0), BREAD_TOP(3, 0, 0.075F, true, 1),
	TOMATO(4, 0, 0F, false, 0), TOMATO_SLICES(5, 0, 0.02F, false, 0), TOMATO_SLICE(8, 0, 0.015F, true, 1),
	BACON(0, 1, 0.015F, true, 3), CHEESE(1, 1, 0.01F, true, 1),
	POTATO(0, 2, 0F, false, 0), SLICED_POTATOES(1, 2, 0F, false, 0), FRIES(2, 2, 0F, false, 6),
	DRINK(0, 3, 0F, false, 4),
	SAUCE_RED(0, 5, 0.015F, false, 1), SAUCE_YELLOW(1, 5, 0.01F, false, 1),
	BURGER(-1, -1, 0F, true, 0),
	PLUS(0, 7, 0F, false, -1), ACTIVE_RECIPE(1, 7, 0F, false, -1), DONE(2, 7, 0F, false, -1),
	DAY_MONDAY(0, 8, 0F, false, -1), DAY_TUESDAY(1, 8, 0F, false, -1), DAY_WEDNESDAY(2, 8, 0F, false, -1), DAY_THURSDAY(3, 8, 0F, false, -1), DAY_FRIDAY(4, 8, 0F, false, -1), DAY_SATURDAY(5, 8, 0F, false, -1), DAY_SUNDAY(6, 8, 0F, false, -1), ;

	private final int x, y;
	private float width = 1, height = 1;
	
	private int burgerX, burgerY;
	private final float burgerHeight;
	private final boolean usePlate;
	private final int price;

	private ItemType(final int x, final int y, final float burgerHeight, final boolean usePlate, final int price) {
		this.x = x;
		this.y = y;
		this.burgerX = x;
		this.burgerY = y;
		this.burgerHeight = burgerHeight;
		this.usePlate = usePlate;
		this.price = price;
	}
	
	static {
		MEAT.burgerX = 0;
		MEAT.burgerY = 0;
		BACON.burgerX = 9;
		BACON.burgerY = 0;
		CHEESE.burgerX = 10;
		CHEESE.burgerY = 0;
		SAUCE_RED.burgerX = 11;
		SAUCE_RED.burgerY = 0;
		SAUCE_YELLOW.burgerX = 12;
		SAUCE_YELLOW.burgerY = 0;
		
		SAUCE_RED.height = 1.5F;
		SAUCE_YELLOW.height = 1.5F;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public float getWidth() {
		return width;
	}
	
	public float getHeight() {
		return height;
	}
	
	public int getBurgerX() {
		return burgerX;
	}
	
	public int getBurgerY() {
		return burgerY;
	}
	
	public boolean isBurgerItem() {
		return burgerHeight > 0F;
	}

	public float getBurgerHeight() {
		return burgerHeight;
	}
	
	public boolean isUsePlate() {
		return usePlate;
	}
	
	public int getBasePrice() {
		return price;
	}
}
