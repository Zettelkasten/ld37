package com.zettelnet.ld37.item;

public class Fries extends Item {

	private final float doneness;
	
	public Fries(SlicedPotatoes potatoes) {
		super(ItemType.FRIES);
		this.doneness = potatoes.getDoneness();
	}
	
	public float getDoneness() {
		System.out.println(doneness);
		return doneness;
	}
	
	@Override
	public float getHappinessFactor() {
		if (doneness > 1) {
			return Math.max((2 - doneness) * 2, 0.1F);
		} else {
			return doneness;
		}
	}
}
