package com.zettelnet.ld37.customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.zettelnet.ld37.item.BurgerItem;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;

public class OrderFactory {

	private static final Random rnd = new Random();

	public ItemType randomType() {
		double a = rnd.nextDouble();
		if (a < 0.5) {
			return ItemType.BURGER;
		} else if (a < 0.8) {
			return ItemType.FRIES;
		} else {
			return ItemType.DRINK;
		}
	}

	public List<Item> makeOrder(int level) {
		List<Item> order = new ArrayList<>();
		int maxSize;
		switch (level) {
		case 0:
			maxSize = 1;
			break;
		case 1:
		case 2:
			maxSize = 2;
			break;
		case 3:
		case 4:
			maxSize = 3;
			break;
		case 5:
		case 6:
			maxSize = 3;
			break;
		default:
			maxSize = 3;
		}

		int size = rnd.nextInt(maxSize) + 1;
		if (size < 4 && rnd.nextDouble() < 0.1) {
			size++;
		}

		for (int i = 0; i < size; i++) {
			ItemType type = randomType();
			order.add(makeItem(level, type));
		}

		return order;
	}

	public Item makeItem(int level, ItemType type) {
		switch (type) {
		case BURGER:
			int maxHeight;
			switch (level) {
			case 0:
				maxHeight = 2;
				break;
			case 1:
				maxHeight = 3;
				break;
			case 2:
				maxHeight = 5;
				break;
			case 3:
				maxHeight = 6;
				break;
			case 4:
				maxHeight = 7;
				break;
			case 5:
				maxHeight = 8;
			case 6:
				maxHeight = 8;
				break;
			default:
				maxHeight = 4;
			}

			int height = rnd.nextInt(maxHeight - 1) + 1;
			List<Item> items = new ArrayList<>();
			items.add(new Item(ItemType.BREAD_BOTTOM));
			for (int i = 0; i < height; i++) {
				switch (rnd.nextInt(6)) {
				case 0:
					items.add(new Item(ItemType.MEAT));
					break;
				case 1:
					items.add(new Item(ItemType.TOMATO_SLICE));
					break;
				case 2:
					items.add(new Item(ItemType.BACON));
					break;
				case 3:
					items.add(new Item(ItemType.CHEESE));
					break;
				case 4:
					items.add(new Item(ItemType.SAUCE_RED));
					break;
				case 5:
					items.add(new Item(ItemType.SAUCE_YELLOW));
					break;
				}
			}
			items.add(new Item(ItemType.BREAD_TOP));
			return new BurgerItem(items);
		case FRIES:
			return new Item(ItemType.FRIES);
		case DRINK:
			return new Item(ItemType.DRINK);
		default:
			return new Item(ItemType.POTATO);
		}
	}
}
