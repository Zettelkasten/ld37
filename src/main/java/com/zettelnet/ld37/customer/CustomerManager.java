package com.zettelnet.ld37.customer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.zettelnet.ld37.item.BurgerItem;
import com.zettelnet.ld37.room.Room;

public class CustomerManager {

	private final Room room;
	private final List<Customer> customerQueue;

	private final int counterSize = 3;
	private final Customer[] counter = new Customer[3];

	private BurgerItem activeRecipe;

	public CustomerManager(final Room room) {
		this.room = room;
		this.customerQueue = new ArrayList<>();
	}

	public void update(float time) {
		// fetch customers if place at counter is available
		if (!customerQueue.isEmpty()) {
			for (int i = 0; i < counterSize; i++) {
				if (counter[i] == null) {
					Customer nextCustomer = customerQueue.remove(0);
					counter[i] = nextCustomer;
					if (customerQueue.isEmpty()) {
						break;
					}
				}
			}
		}

		if (activeCustomers() == 0) {
			customerQueue.add(new Customer(this));
		}
		float factor = 1F;
		switch (room.getLevel()) {
		case 0:
		case 1:
			factor = 1;
			break;
		case 2:
		case 3:
		case 4:
			factor = 1.2F;
			break;
		case 5:
		case 6:
			factor = 1.5F;
			break;
		}
		
		if (customerQueue.isEmpty()) {
			if (Math.random() < 0.005F * factor) {
				customerQueue.add(new Customer(this));
			}
		} else {
			if (Math.random() < 0.004F * factor) {
				customerQueue.add(new Customer(this));
			}
		}

		for (int i = 0; i < counterSize; i++) {
			Customer customer = counter[i];
			if (customer != null) {
				customer.update(time);
				if (customer.shouldDismiss()) {
					customer.onDismiss();
					counter[i] = null;
				}
			}
		}

		for (Iterator<Customer> i = customerQueue.iterator(); i.hasNext();) {
			Customer customer = i.next();
			customer.update(time);
			if (customer.shouldDismiss()) {
				customer.onDismiss();
				i.remove();
			}
		}
	}

	public boolean hasCustomer(int counterSlot) {
		return getCustomer(counterSlot) != null;
	}

	public Customer getCustomer(int counterSlot) {
		if (counterSlot >= counterSize) {
			if (counterSlot >= customerQueue.size() - counterSize) {
				return null;
			} else {
				return customerQueue.get(counterSlot - counterSize);
			}
		} else {
			return counter[counterSlot];
		}
	}

	public int activeCustomers() {
		int count = 0;
		for (int i = 0; i < counterSize; i++) {
			if (hasCustomer(i)) {
				count++;
			}
		}
		return count;
	}

	public void setActiveRecipe(BurgerItem activeRecipe) {
		this.activeRecipe = activeRecipe;
	}

	public BurgerItem getActiveRecipe() {
		return activeRecipe;
	}
	
	public Room getRoom() {
		return room;
	}

	public void clear() {
		for (int i = 0; i < counterSize; i++) {
			counter[i] = null;
		}
		customerQueue.clear();
		activeRecipe = null;
	}
}
