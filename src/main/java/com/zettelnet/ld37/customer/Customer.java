package com.zettelnet.ld37.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.zettelnet.ld37.item.Item;

public class Customer {

	private static final Random rnd = new Random();

	private final CustomerManager customerManager;
	private static final OrderFactory orderFactory = new OrderFactory();

	private final List<Item> order;
	private final Map<Item, Item> completedOrder;

	private final int eyes = rnd.nextInt(5);
	private final int nose = rnd.nextInt(5);

	private boolean accepted = false;
	private boolean complete = false;

	private float patience = 1F;
	private float angreeness = 0F;
	private float happiness;
	private int hasPaid;

	private float completeCooldown = 0F;
	private float completeCooldownMax = 20F;

	private boolean shouldDismiss = false;

	public Customer(CustomerManager customerManager) {
		this.customerManager = customerManager;

		this.order = orderFactory.makeOrder(customerManager.getRoom().getLevel());
		this.completedOrder = new HashMap<>();
		this.patience = order.size() / 2F + 0.8F;
	}

	public void update(float time) {
		if (complete) {
			completeCooldown += time;
			if (completeCooldown >= completeCooldownMax) {
				shouldDismiss = true;
			}
		} else {
			angreeness += time / 6000F * patience;
			if (angreeness >= 1) {
				shouldDismiss = true;
			}
		}
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public List<Item> getOrder() {
		return order;
	}

	public Item getCompletedOrder(Item requested) {
		return completedOrder.get(requested);
	}

	public boolean isItemComplete(int i) {
		return completedOrder.containsKey(order.get(i));
	}

	public void setItemComplete(int item, Item used) {
		completedOrder.put(order.get(item), used);
		if (order.get(item) == customerManager.getActiveRecipe()) {
			customerManager.setActiveRecipe(null);
		}

		// check if done
		for (int i = 0; i < order.size(); i++) {
			if (!isItemComplete(i)) {
				return;
			}
		}
		complete = true;
		onComplete();
	}

	public boolean shouldDismiss() {
		return shouldDismiss;
	}

	public float getAngreeness() {
		return angreeness;
	}

	public int getEyes() {
		return eyes;
	}

	public int getNose() {
		return nose;
	}

	public int getMouth() {
		return Math.max(Math.min((int) (angreeness * 5), 4), 0);
	}

	public void onDismiss() {
		for (Item item : order) {
			if (customerManager.getActiveRecipe() == item) {
				customerManager.setActiveRecipe(null);
			}
		}
	}

	public void onComplete() {
		// calculate happiness
		happiness = 1.5F - angreeness;
		for (Item item : order) {
			Item got = getCompletedOrder(item);
			happiness *= got.getHappinessFactor();
		}
		angreeness = 1F - happiness;
		
		// calculate hasPaid
		hasPaid = 1;
		for (Item item : order) {
			hasPaid += item.getPrice();
		}
		hasPaid *= happiness;
		customerManager.getRoom().increaseMoney(hasPaid);
	}
	
	public float getCompleteCooldown() {
		return completeCooldown;
	}

	public boolean isComplete() {
		return complete;
	}

	public float getHappiness() {
		return happiness;
	}

	public int getPaid() {
		return hasPaid;
	}
}
