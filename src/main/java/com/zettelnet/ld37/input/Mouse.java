package com.zettelnet.ld37.input;

import java.nio.DoubleBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

import com.zettelnet.ld37.render.GameRenderer;
import com.zettelnet.ld37.render.MouseCallback;
import com.zettelnet.ld37.room.Device;
import com.zettelnet.ld37.room.Room;

import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;

public class Mouse implements MouseCallback {

	private final Room room;
	
	public Mouse(final Room room) {
		this.room = room;
	}
	
	@Override
	public void invoke(long window, double mouseX, double mouseY, int button, int action, int mods) {
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
			double x = mouseToX(mouseX, GameRenderer.WINDOW_WIDTH);
			double y = mouseToY(mouseY, GameRenderer.WINDOW_HEIGHT);
			
			Device device = room.getDeviceAt(x, y);
			if (device != null) {
				device.onClick(x - device.getX(), y - device.getY());
			}
		}

		if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {

		}
	}

	public static double mouseToX(double mouseX, int maxWidth) {
		return mouseX / maxWidth * 2 - 1;
	}

	public static double mouseToY(double mouseY, int maxHeight) {
		return -mouseY / maxHeight * 2 + 1;
	}
	
	public static double getPosX() {
		DoubleBuffer posX = BufferUtils.createDoubleBuffer(1);
		GLFW.glfwGetCursorPos(GameRenderer.WINDOW, posX, null);
		return mouseToX(posX.get(0), GameRenderer.WINDOW_WIDTH);
	}
	
	public static double getPosY() {
		DoubleBuffer posY = BufferUtils.createDoubleBuffer(1);
		GLFW.glfwGetCursorPos(GameRenderer.WINDOW, null, posY);
		return mouseToY(posY.get(0), GameRenderer.WINDOW_HEIGHT);
	}
}
