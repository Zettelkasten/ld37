package com.zettelnet.ld37;

public class MathUtil {

	public static float angle(float x, float y) {
		float rotation = (float) (Math.atan(y / x) + Math.PI / 2);
		if (x < 0) {
			rotation -= (float) (Math.PI);
		}
		return rotation;
	}
}
