package com.zettelnet.ld37.room;

import java.util.List;

import com.zettelnet.ld37.customer.Customer;
import com.zettelnet.ld37.customer.CustomerManager;
import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.BurgerItem;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.render.ItemRenderer;

public class OrderSlot extends Device {

	public static OrderSlot makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight, CustomerManager customerManager, int counterSlot) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new OrderSlot(room, x, y, width, height, customerManager, counterSlot);
	}

	private final CustomerManager customerManager;
	private final int counterSlot;

	private static final Item ACTIVE_RECIPE = new Item(ItemType.ACTIVE_RECIPE);
	private static final Item DONE = new Item(ItemType.DONE);
	
	public OrderSlot(Room room, double x, double y, double width, double height, CustomerManager customerManager, int counterSlot) {
		super(room, x, y, width, height);
		this.customerManager = customerManager;
		this.counterSlot = counterSlot;
	}

	@Override
	public void onClick(double relX, double relY) {
		Customer customer = customerManager.getCustomer(counterSlot);

		if (customer != null) {
			if (!customer.isAccepted()) {
				customer.setAccepted(true);
			} else {
				double x = relX + getWidth() / 2;
				int segment = (int) (x / getWidth() * 4);
				if (customer.getOrder().size() > segment) {
					Item item = customer.getOrder().get(segment);
					if (item.getType() == ItemType.BURGER) {
						customerManager.setActiveRecipe((BurgerItem) item);
					}
				}
			}
		}
	}

	@Override
	public void render(double time) {
		Customer customer = customerManager.getCustomer(counterSlot);

		if (customer != null) {
			float angreeness = customer.getAngreeness();
			if (customer.isAccepted()) {
				getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 1038, 0, 340, 120, 0, 0, angreeness);
				List<Item> order = customer.getOrder();
				ItemRenderer itemRenderer = getRoom().getGame().getItemRenderer();
				float offsetStep = 0.08F;
				float offset = -2 * offsetStep + 0.05F;

				for (Item item : order) {
					itemRenderer.renderItem(time, item, (float) getX() + offset, (float) getY());
					if (customer.getCompletedOrder(item) != null) {
						itemRenderer.renderItem(time, DONE, (float) getX() + offset, (float) getY());
					} else 
					if (customerManager.getActiveRecipe() == item) {
						itemRenderer.renderItem(time, ACTIVE_RECIPE, (float) getX() + offset, (float) getY());
					}
					offset += offsetStep;
				}
			} else {
				getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 1038, 120, 340, 120, 0, 0, angreeness);
			}
		}
	}

	@Override
	public void update(float time) {
	}
}
