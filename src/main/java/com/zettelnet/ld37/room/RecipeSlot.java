package com.zettelnet.ld37.room;

import java.util.List;

import com.zettelnet.ld37.customer.CustomerManager;
import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.BurgerItem;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.render.ItemRenderer;

public class RecipeSlot extends Device {

	public static RecipeSlot makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight, CustomerManager customerManager) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new RecipeSlot(room, x, y, width, height, customerManager);
	}

	private final CustomerManager customerManager;
	private static final Item PLUS = new Item(ItemType.PLUS);

	public RecipeSlot(Room room, double x, double y, double width, double height, CustomerManager customerManager) {
		super(room, x, y, width, height);
		this.customerManager = customerManager;
	}

	@Override
	public void onClick(double relX, double relY) {
	}

	@Override
	public void render(double time) {
		BurgerItem burgerRecipe = customerManager.getActiveRecipe();
		if (burgerRecipe != null) {
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 1038, 120 * 2, 470, 200, 0, 0);
			
			List<Item> recipe = burgerRecipe.getItems();
			int size = recipe.size();

			final float itemSize = 0.05F;
			final float plusSize = 0.05F;
			final ItemRenderer itemRenderer = getRoom().getGame().getItemRenderer();
			
			int splitAt = size / 2;
			
			float totalTopSize = itemSize * (splitAt) + plusSize * (splitAt);
			float totalBottomSize = itemSize * (size - splitAt) + plusSize * (size - 1 - splitAt);

			// top
			
			float x = (float) (getX() - getWidth() / 2 + (getWidth() - totalTopSize) / 2);
			x += itemSize / 2;			
			
			for (int i = 0; i < splitAt; i++) {
				Item item = recipe.get(i);
				itemRenderer.renderItem(time, item, x, (float) getY() + 0.08F);
				x += itemSize;
				if (i != size - 1) {
					itemRenderer.renderItem(time, PLUS, x, (float) getY() + 0.08F);
					x += plusSize;
				}
			}
			
			// bottom
			
			x = (float) (getX() - getWidth() / 2 + (getWidth() - totalBottomSize) / 2);
			x += itemSize / 2;			
			
			for (int i = splitAt; i < size; i++) {
				Item item = recipe.get(i);
				itemRenderer.renderItem(time, item, x, (float) getY() - 0.08F);
				x += itemSize;
				if (i != size - 1) {
					itemRenderer.renderItem(time, PLUS, x, (float) getY() - 0.08F);
					x += plusSize;
				}
			}
		}
	}

	@Override
	public void update(float time) {
	}
}
