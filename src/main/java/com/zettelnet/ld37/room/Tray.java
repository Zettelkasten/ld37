package com.zettelnet.ld37.room;

import java.util.List;

import com.zettelnet.ld37.customer.Customer;
import com.zettelnet.ld37.customer.CustomerManager;
import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.Items;
import com.zettelnet.ld37.render.ItemRenderer;

public class Tray extends Device {

	public static Tray makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight, CustomerManager customerManager, int counterSlot) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Tray(room, x, y, width, height, customerManager, counterSlot);
	}

	private final CustomerManager customerManager;
	private final int counterSlot;

	public Tray(Room room, double x, double y, double width, double height, CustomerManager customerManager, int counterSlot) {
		super(room, x, y, width, height);
		this.customerManager = customerManager;
		this.counterSlot = counterSlot;
	}

	@Override
	public void onClick(double relX, double relY) {
		if (customerManager.hasCustomer(counterSlot)) {
			Customer customer = customerManager.getCustomer(counterSlot);
			if (customer.isAccepted()) {
				Item inventory = getRoom().getInventory();
				List<Item> order = customer.getOrder();
				for (int i = 0; i < order.size(); i++) {
					if (!customer.isItemComplete(i)) {
						Item item = order.get(i);
						if (Items.equals(item, inventory)) {
							customer.setItemComplete(i, inventory);
							getRoom().setInventory(null);
							return;
						}
					}
				}
			}
		}
	}

	@Override
	public void render(double time) {
		if (customerManager.hasCustomer(counterSlot)) {
			Customer customer = customerManager.getCustomer(counterSlot);
			if (customer.isAccepted()) {
				getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 558, 0, 160, 120);

				ItemRenderer itemRenderer = getRoom().getGame().getItemRenderer();

				float xOffset = 0.035F;
				float yOffset = 0.08F;

				List<Item> items = customer.getOrder();

				if (items.size() > 0 && customer.isItemComplete(0)) {
					itemRenderer.renderItem(time, customer.getCompletedOrder(items.get(0)), (float) getX() - xOffset, (float) getY() + yOffset);
				}
				if (items.size() > 1 && customer.isItemComplete(1)) {
					itemRenderer.renderItem(time, customer.getCompletedOrder(items.get(1)), (float) getX() + xOffset, (float) getY() + yOffset);
				}
				if (items.size() > 2 && customer.isItemComplete(2)) {
					itemRenderer.renderItem(time, customer.getCompletedOrder(items.get(2)), (float) getX() - xOffset, (float) getY() - yOffset + 0.08F);
				}
				if (items.size() > 3 && customer.isItemComplete(3)) {
					itemRenderer.renderItem(time, customer.getCompletedOrder(items.get(3)), (float) getX() + xOffset, (float) getY() - yOffset + 0.08F);
				}
			}
		}
	}

	@Override
	public void update(float time) {
	}
}
