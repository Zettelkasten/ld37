package com.zettelnet.ld37.room;

public abstract class Device {

	private final Room room;
	private final double x, y;
	private  double width, height;

	public Device(final Room room, final double x, final double y, final double width, final double height) {
		this.room = room;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		System.out.println("Spawned device at " + x + ":" + y + " with dimension " + width + ":" + height);
	}

	public Room getRoom() {
		return room;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	// public abstract void render();

	public boolean isAt(double locX, double locY) {
//		this.width = Mouse.mouseToX(200, 1920) + 1;
//		this.height = Mouse.mouseToY(1080 - 270, 1080) + 1;
		return this.x - this.width / 2 <= locX && locX <= this.x + this.width / 2
				&& this.y - this.height / 2<= locY && locY <= this.y + this.height / 2;
	}
	
	public abstract void onClick(double relX, double relY);
	
	public abstract void render(double time);

	public abstract void update(float time);
}
