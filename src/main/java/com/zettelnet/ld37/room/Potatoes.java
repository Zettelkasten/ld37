package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;

public class Potatoes extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Potatoes(room, x, y, width, height);
	}
	
	public Potatoes(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	@Override
	public void onClick(double relX, double relY) {
		Room room = getRoom();
		if (room.getInventory() == null) {
			room.setInventory(new Item(ItemType.POTATO));
		} else if (room.getInventory().getType() == ItemType.POTATO) {
			room.setInventory(null);
		}
	}

	@Override
	public void render(double time) {
		getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 119, 0, 119, 125);
	}

	@Override
	public void update(float time) {
	}
}
