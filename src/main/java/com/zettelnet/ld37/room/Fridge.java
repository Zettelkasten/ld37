package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.BaconItem;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.item.MeatItem;
import com.zettelnet.ld37.render.ItemRenderer;

public class Fridge extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Fridge(room, x, y, width, height);
	}

	private static Item MEAT = new MeatItem();
	private static Item CHEESE = new Item(ItemType.CHEESE);
	private static Item BACON = new BaconItem();
	private static Item DRINK = new Item(ItemType.DRINK);

	private float closeTimer = 0;
	private final float maxAutoClose = 50;

	public Fridge(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	public boolean isOpen() {
		return closeTimer > 0;
	}
	
	@Override
	public void onClick(double relX, double relY) {
		if (isOpen()) {
			Room room = getRoom();
			if (room.getInventory() == null) {
				room.setInventory(makeClickedItem(relX, relY));
			} else {
				ItemType type = room.getInventory().getType();
				switch (type) {
				case BACON:
				case CHEESE:
				case DRINK:
				case MEAT:
					room.setInventory(null);
					break;
				default:
					break;
				}
			}
		}
		
		closeTimer = maxAutoClose;
	}
	
	public Item makeClickedItem(double relX, double relY) {
		if (relX < 0 && relY > 0) {
			return new MeatItem();
		} else if (relX > 0 && relY > 0) {
			return new Item(ItemType.CHEESE);
		} else if (relX < 0 && relY < 0) {
			return new BaconItem();
		} else if (relX > 0 && relY < 0) {
			return new Item(ItemType.DRINK);
		} else {
			return null;
		}
	}

	@Override
	public void render(double time) {
		if (isOpen()) {
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 398, 0, 160, 234);

			ItemRenderer itemRenderer = getRoom().getGame().getItemRenderer();

			float xOffset = 0.035F;
			float yOffset = 0.08F;

			itemRenderer.renderItem(time, MEAT, (float) getX() - xOffset, (float) getY() + yOffset);
			itemRenderer.renderItem(time, CHEESE, (float) getX() + xOffset, (float) getY() + yOffset);
			itemRenderer.renderItem(time, BACON, (float) getX() - xOffset, (float) getY() - yOffset);
			itemRenderer.renderItem(time, DRINK, (float) getX() + xOffset, (float) getY() - yOffset);
		}
	}

	@Override
	public void update(float time) {
		closeTimer -= time;
		if (closeTimer < 0) {
			closeTimer = 0;
		}
	}
}
