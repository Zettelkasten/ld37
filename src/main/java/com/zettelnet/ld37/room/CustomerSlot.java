package com.zettelnet.ld37.room;

import com.zettelnet.ld37.customer.Customer;
import com.zettelnet.ld37.customer.CustomerManager;
import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.zetlib.util.Math2D;

public class CustomerSlot extends Device {

	public static CustomerSlot makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight, CustomerManager customerManager, int counterSlot) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new CustomerSlot(room, x, y, width, height, customerManager, counterSlot);
	}

	private final CustomerManager customerManager;
	private final int counterSlot;

	private float moneySpeed = 0.005F;

	public CustomerSlot(Room room, double x, double y, double width, double height, CustomerManager customerManager, int counterSlot) {
		super(room, x, y, width, height);
		this.customerManager = customerManager;
		this.counterSlot = counterSlot;
	}

	@Override
	public void onClick(double relX, double relY) {
		Customer customer = customerManager.getCustomer(counterSlot);

		if (customer != null) {
			if (!customer.isAccepted()) {
				customer.setAccepted(true);
			}
		}
	}

	@Override
	public void render(double time) {
		Customer customer = customerManager.getCustomer(counterSlot);

		if (customer != null) {
			int originX = 558;
			int originY = 120;
			int width = 160;
			int height = 120;

			if (customer.isComplete()) {
				getRoom().getGame().getDeviceRenderer().renderDevice(time, this, originX + 0 * width, originY + 5 * height, width, height, 0, 0, customer.getAngreeness());
			}

			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, originX + 0 * width, originY + customer.getEyes() * height, width, height);
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, originX + 1 * width, originY + customer.getNose() * height, width, height);
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, originX + 2 * width, originY + customer.getMouth() * height, width, height);

			// money
			if (customer.isComplete()) {
				getRoom().getGame().getDeviceRenderer().renderNumber(time, (float) getX() - 0.05F, (float) Math2D.lerp(getY() + customer.getCompleteCooldown() * moneySpeed, getY() + (customer.getCompleteCooldown() + 1) * moneySpeed, time) + 0.1F, "+" + customer.getPaid()+"$");
			}
		}
	}

	@Override
	public void update(float time) {
	}
}
