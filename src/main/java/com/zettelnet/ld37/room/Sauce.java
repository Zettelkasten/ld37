package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;

public class Sauce extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight, Item startItem) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Sauce(room, x, y, width, height, startItem);
	}

	private Item item;

	public Sauce(Room room, double x, double y, double width, double height, Item startItem) {
		super(room, x, y, width, height);
		this.item = startItem;
	}

	@Override
	public void onClick(double relX, double relY) {
		Room room = getRoom();
		if (room.getInventory() == null) {
			room.setInventory(item);
			item = null;
		} else if (room.getInventory().getType() == ItemType.SAUCE_YELLOW || room.getInventory().getType() == ItemType.SAUCE_RED) {
			if (item == null) {
				item = room.getInventory();
				room.setInventory(null);
			}
		}
	}

	@Override
	public void render(double time) {
		if (item != null) {
			getRoom().getGame().getItemRenderer().renderItem(time, item, (float) getX(), (float) getY());
		}
	}

	@Override
	public void update(float time) {
	}
}
