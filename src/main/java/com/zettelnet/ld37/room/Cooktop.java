package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.Roastable;

public class Cooktop extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Cooktop(room, x, y, width, height);
	}

	private Roastable item = null;

	public Cooktop(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	@Override
	public void onClick(double relX, double relY) {
		Room room = getRoom();
		Item inventory = room.getInventory();

		if (item == null) {
			// could place item
			if (inventory != null && inventory instanceof Roastable) {
				room.setInventory(null);
				this.item = (Roastable) inventory;
			}
		} else {
			if (inventory == null) {
				room.setInventory((Item) item);
				this.item = null;
			}
		}
	}

	@Override
	public void render(double time) {
		if (item != null) {
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 0, 0, 119, 112);
			getRoom().getGame().getItemRenderer().renderItem(time, (Item) item, (float) getX(), (float) getY());
			
			// progress
			int stage = (int) (17 * item.getDoneness());
			if (stage > 17) {
				int overStage = stage - 17;
				stage = 17 + overStage / 5;
				stage = Math.min(stage, 21);
			}
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 0, 112 + stage * 35, 119, 35, 0, -0.09F);
		}
	}

	@Override
	public void update(float time) {
		if (item != null) {
			item.increaseDoneness(0.005F);
		}
	}
}
