package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Fries;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.item.SlicedPotatoes;

public class FriesBox extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new FriesBox(room, x, y, width, height);
	}
	
	public FriesBox(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	@Override
	public void onClick(double relX, double relY) {
		Room room = getRoom();
		Item inventory = room.getInventory();
		if (inventory != null && inventory.getType() == ItemType.SLICED_POTATOES) {
			Fries fries = new Fries((SlicedPotatoes) inventory);
			room.setInventory(fries);
		}
	}

	@Override
	public void render(double time) {
	}

	@Override
	public void update(float time) {
	}
}
