package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.item.SlicedPotatoes;
import com.zettelnet.ld37.item.TomatoSlices;

public class CuttingBoard extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new CuttingBoard(room, x, y, width, height);
	}

	public CuttingBoard(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	private ItemType type;
	private int amount;
	private final int maxAmount = 3;

	@Override
	public void onClick(double relX, double relY) {
		Room room = getRoom();
		Item inventory = room.getInventory();
		if (amount == maxAmount) {
			if (inventory == null) {
				if (type == ItemType.POTATO) {
					room.setInventory(new SlicedPotatoes());
				} else if (type == ItemType.TOMATO) {
					room.setInventory(new TomatoSlices());
				}
				amount = 0;
				type = null;
			}
		} else {
			if (inventory != null) {
				if (this.type == null && inventory.getType() == ItemType.POTATO) {
					this.type = ItemType.POTATO;
					amount = 1;
					room.setInventory(null);
				} else if (this.type == null && inventory.getType() == ItemType.TOMATO) {
					this.type = ItemType.TOMATO;
					amount = 3;
					room.setInventory(null);
				} else if (inventory.getType() == this.type) {
					room.setInventory(null);
					amount++;
				}
			}
		}
	}

	@Override
	public void render(double time) {
		if (amount > 0) {
			int offset = 0;
			if (type == ItemType.POTATO) {
				offset = 0 + amount - 1;
			} else if (type == ItemType.TOMATO) {
				offset = 3;
			}
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 239, offset * 100, 160, 100);
		}
	}

	@Override
	public void update(float time) {
	}
}
