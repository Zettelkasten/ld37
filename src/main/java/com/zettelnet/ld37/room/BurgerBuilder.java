package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;
import com.zettelnet.ld37.item.BurgerItem;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;
import com.zettelnet.ld37.item.TomatoSlices;

public class BurgerBuilder extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new BurgerBuilder(room, x, y, width, height);
	}

	private Item item = null;
	private boolean burgerMode = false;

	public BurgerBuilder(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	@Override
	public void onClick(double relX, double relY) {
		Item inventory = getRoom().getInventory();
		if (inventory != null) {
			if (item == null) {
				if (inventory.getType() == ItemType.BREAD) {
					this.item = new BurgerItem(new Item(ItemType.BREAD_BOTTOM));
					burgerMode = true;
					getRoom().setInventory(null);
				} else if (inventory.getType() == ItemType.BURGER) {
					this.item = inventory;
					burgerMode = true;
					getRoom().setInventory(null);
				} else {
					item = inventory;
					getRoom().setInventory(null);
				}
			} else {
				if (inventory.getType().isBurgerItem() && burgerMode) {
					BurgerItem burger = (BurgerItem) item;
					if (!burger.isDone()) {
						if (inventory.getType() == ItemType.BREAD) {
							burger.add(new Item(ItemType.BREAD_TOP));
							getRoom().setInventory(null);
						} else if (inventory.getType() == ItemType.TOMATO_SLICES) {
							TomatoSlices slices = (TomatoSlices) inventory;
							if (slices.getAmount() == 1) {
								getRoom().setInventory(null);
							} else {
								slices.setAmount(slices.getAmount() - 1);
							}
							burger.add(new Item(ItemType.TOMATO_SLICE));
						} else if (inventory.getType() == ItemType.SAUCE_RED || inventory.getType() == ItemType.SAUCE_YELLOW) {
							burger.add(inventory);
						} else {
							burger.add(inventory);
							getRoom().setInventory(null);
						}
					}
				}
			}
		} else {
			getRoom().setInventory(item);
			burgerMode = false;
			item = null;
		}
	}

	@Override
	public void render(double time) {
		if (item != null && item.getType().isUsePlate()) {
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 163, 125, 75, 75, 0, -0.025F);
		}

		if (item != null) {
			getRoom().getGame().getItemRenderer().renderItem(time, item, (float) getX(), (float) getY());
		}
	}

	@Override
	public void update(float time) {
	}
}
