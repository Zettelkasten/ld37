package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;

public class Clock extends Device {

	public static Device makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Clock(room, x, y, width, height);
	}

	public Clock(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	@Override
	public void onClick(double relX, double relY) {
		getRoom().getGame().setShowHelp(!getRoom().getGame().isShowHelp());
	}

	private float time = (float) (2 * Math.PI * -3);

	@Override
	public void render(double time) {
		float hourAngle = -this.time / 12F;
		float minAngle = -this.time;

		getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 398, 240, 160, 160);
		getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 398, 240 + 160 * 1, 160, 160, 0, 0, 0, hourAngle);
		getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 398, 240 + 160 * 2, 160, 160, 0, 0, 0, minAngle);

		if (getRoom().getGame().isShowHelp()) {
			getRoom().getGame().getDeviceRenderer().renderDevice(time, this, 398, 240 + 160 * 3, 160, 160, 0.01F, 0);
		}

		getRoom().getGame().getDeviceRenderer().renderNumber(time, (float) getX(), (float) getY() + 0.2F, getRoom().getMoney() + "$");
		getRoom().getGame().getDeviceRenderer().renderNumber(time, (float) getX(), (float) getY() - 0.2F, "(" + (int) (((float) getRoom().getMoney() / getRoom().getMoneyGoal() * 100)) + "%)");
		
		getRoom().getGame().getItemRenderer().renderItem(time, Room.LEVEL_ITEMS[getRoom().getLevel()], (float) getX() + 0.1F, (float) getY() - 0.1F);
	}

	@Override
	public void update(float changeTime) {
		this.time += changeTime / 50F;

		if (this.time >= 2 * Math.PI * 7) {
			// close
			getRoom().setTimeOver(true);
			time = (float) (2 * Math.PI * - 3);
		}
	}
}
