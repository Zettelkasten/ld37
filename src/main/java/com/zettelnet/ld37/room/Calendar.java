package com.zettelnet.ld37.room;

import com.zettelnet.ld37.input.Mouse;

public class Calendar extends Device {

	public static Calendar makeHelper(Room room, int x1, int y1, int pixelWidth, int pixelHeight) {
		int maxWidth = 1920, maxHeight = 1080;
		double x = Mouse.mouseToX(x1, maxWidth);
		double y = Mouse.mouseToY(y1, maxHeight);
		double width = Mouse.mouseToX(pixelWidth, maxWidth) + 1;
		double height = Mouse.mouseToY(maxHeight - pixelHeight, maxHeight) + 1;
		return new Calendar(room, x, y, width, height);
	}

	public Calendar(Room room, double x, double y, double width, double height) {
		super(room, x, y, width, height);
	}

	@Override
	public void onClick(double relX, double relY) {
		if (getRoom().isDefeat()) {
			getRoom().setDefeat(false);
		} else if (!getRoom().isWon()) {
			getRoom().getGame().setShowCalendar(false);
			getRoom().setDefeat(false);
			getRoom().setMoney(0);
			getRoom().setMoneyGoal(Room.MONEY_GOALS[getRoom().getLevel()]);
		}
	}

	@Override
	public void render(double time) {
		// rendered using HelpRenderer
	}

	@Override
	public void update(float changeTime) {
	}

	@Override
	public boolean isAt(double locX, double locY) {
		if (!getRoom().getGame().isShowCalendar()) {
			return false;
		}
		return super.isAt(locX, locY);
	}
}
