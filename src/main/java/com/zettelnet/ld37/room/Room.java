package com.zettelnet.ld37.room;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.zettelnet.ld37.Game;
import com.zettelnet.ld37.item.Item;
import com.zettelnet.ld37.item.ItemType;

public class Room {

	private final Game game;
	private final List<Device> devices;

	public static final int[] MONEY_GOALS = {
			60,
			150,
			200,
			250,
			300,
			400,
			500
	};
	
	private Item inventory = null;
	private int money = 0;
	private int moneyGoal = MONEY_GOALS[0];
	
	private boolean won = false;
	private boolean defeat = false;
	
	private int level = 0;
	public static Item[] LEVEL_ITEMS = {
			new Item(ItemType.DAY_MONDAY),
			new Item(ItemType.DAY_TUESDAY),
			new Item(ItemType.DAY_WEDNESDAY),
			new Item(ItemType.DAY_THURSDAY),
			new Item(ItemType.DAY_FRIDAY),
			new Item(ItemType.DAY_SATURDAY),
			new Item(ItemType.DAY_SUNDAY)
	};

	public Room(final Game game) {
		this.game = game;
		this.devices = new ArrayList<>();
	}

	public <T extends Device> T addDevice(final T device) {
		this.devices.add(device);
		return device;
	}

	public Device getDeviceAt(double x, double y) {
		for (Device device : devices) {
			if (device.isAt(x, y)) {
				return device;
			}
		}
		return null;
	}

	public Collection<Device> getDevices() {
		return devices;
	}

	public Item getInventory() {
		return inventory;
	}

	public void setInventory(Item newItem) {
		this.inventory = newItem;
	}

	public Game getGame() {
		return game;
	}

	public void update(float time) {
		for (Device device : devices) {
			device.update(time);
		}
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public void increaseMoney(int amount) {
		this.money += amount;
	}

	public int getMoneyGoal() {
		return moneyGoal;
	}
	
	public int getLevel() {
		return level;
	}

	public void setTimeOver(boolean b) {
		if (b) {
			if (money >= moneyGoal) {
				onVictory();
			} else {
				onDefeat();
			}
//			game.getCustomerManager().clear();
		}
	}
	
	public void onVictory() {
		level++;
		if (level == 7) {
			won = true;
		}
		game.setShowCalendar(true);
	}
	
	public void onDefeat() {
		defeat = true;
		game.setShowCalendar(true);
	}
	
	public boolean isDefeat() {
		return defeat;
	}
	
	public void setDefeat(boolean defeat) {
		this.defeat = defeat;
	}
	
	public boolean isWon() {
		return won;
	}

	public void setMoneyGoal(int i) {
		this.moneyGoal = i;
	}
}
