#version 400 core

in vec2 pass_textureCoords;

out vec4 out_Color;

uniform sampler2D tex;
uniform vec2 textureOffset;

void main(void) {
	vec4 outColor = texture(tex, pass_textureCoords);
	
	out_Color = outColor;
}