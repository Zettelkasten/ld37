#version 400 core

in vec2 pass_textureCoords;

out vec4 out_Color;

uniform sampler2D tex;
uniform vec2 textureSize;
uniform vec2 textureOffset;
uniform float brightness;

vec4 grayscale(vec4 color) {
	float main = 0.2989 * color.r + 0.5870 * color.g + 0.1140 * color.b;
	return vec4(main, main, main, color.a);
}

void main(void) {
	vec4 outColor = texture(tex, pass_textureCoords * textureSize + textureOffset);
	
	out_Color = outColor += outColor.a * brightness;
}
